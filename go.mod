module tmp/wpodns

go 1.14

require (
	github.com/miekg/dns v1.1.29
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
)
