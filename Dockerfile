FROM golang:alpine AS build
COPY main.go /go/src
COPY go.sum /go/src
COPY go.mod /go/src
RUN cd /go/src && go mod vendor && go build -o wpodns

FROM alpine:3.7
COPY --from=build /go/src/wpodns /wpodns

ENTRYPOINT ["/wpodns"]
