package main

import (
	"encoding/json"
	"net"
	"net/http"
	"net/url"
	"strings"

	"github.com/miekg/dns"
	"github.com/op/go-logging"
)

var logger *logging.Logger

func loggerCreate() {
	format := logging.MustStringFormatter("[%{level}] %{shortfile} %{shortfunc}(): %{message}")
	logging.SetFormatter(format)
	logging.SetLevel(logging.DEBUG, "")
	logger = logging.MustGetLogger("wpodns")
}

func getTitle(q string) (string, error) {
	u, err := url.Parse("https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=&srprop=snippet&srlimit=1&format=json")
	if err != nil {
		logger.Errorf("error parsing URL: %v", err)
		return "", err
	}
	query := u.Query()
	query.Set("srsearch", q)
	u.RawQuery = query.Encode()
	resp, err := http.Get(u.String())
	if err != nil {
		logger.Errorf("error performing GET: %v", err)
		return "", err
	}
	defer resp.Body.Close()

	var decodedResp struct {
		Query struct {
			Search []struct {
				Title string `json"title"`
			} `json:"search"`
		} `json:"query"`
	}
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&decodedResp); err != nil {
		logger.Errorf("error decoding JSON: %v", err)
		return "", err
	}

	if len(decodedResp.Query.Search) == 0 {
		return q, nil
	}
	return decodedResp.Query.Search[0].Title, nil
}

func getExtract(q string) (string, error) {
	title, err := getTitle(q)
	if err != nil {
		logger.Errorf("error getting title: %v", err)
		return "", nil
	}

	u, err := url.Parse("https://en.wikipedia.org/w/api.php?action=query&prop=extracts&titles=&explaintext=1&format=json&exchars=120")
	if err != nil {
		logger.Errorf("error parsing URL: %v", err)
		return "", err
	}
	query := u.Query()
	query.Set("titles", title)
	u.RawQuery = query.Encode()
	resp, err := http.Get(u.String())
	if err != nil {
		logger.Errorf("error performing GET: %v", err)
		return "", err
	}
	defer resp.Body.Close()

	var decodedResp struct {
		Query struct {
			Pages map[string]struct {
				Extract string `json:"extract"`
			} `json:"pages"`
		} `json:"query"`
	}
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&decodedResp); err != nil {
		logger.Errorf("error decoding JSON: %v", err)
		return "", err
	}

	for _, r := range decodedResp.Query.Pages {
		return r.Extract, nil
	}
	return "<empty>", nil
}

func serve() {
	mux := dns.NewServeMux()
	mux.HandleFunc("wpodns.adtac.in.", func(w dns.ResponseWriter, r *dns.Msg) {
		m := new(dns.Msg)
		m.SetReply(r)

		for _, q := range r.Question {
			switch q.Qtype {
			case dns.TypeA:
				if q.Name == "wpodns.adtac.in." {
					m.Answer = append(m.Answer, &dns.A{Hdr: dns.RR_Header{
						Name:   q.Name,
						Rrtype: dns.TypeA,
						Class:  dns.ClassINET,
						Ttl:    3600,
					}, A: net.ParseIP("148.251.77.168")})
				}

			case dns.TypeTXT:
				query := q.Name[:strings.Index(q.Name, ".")]
				logger.Debugf("new query '%s'", query)
				extract, err := getExtract(query)
				if err != nil {
					logger.Errorf("cannot get extract: %v", err)
					extract = "<error>"
				}
				if len(extract) > 126 {
					extract = extract[:126]
				}
				m.Answer = append(m.Answer, &dns.TXT{Hdr: dns.RR_Header{
					Name:   q.Name,
					Rrtype: dns.TypeTXT,
					Class:  dns.ClassINET,
					Ttl:    3600,
				}, Txt: []string{extract}})
			}
		}

		w.WriteMsg(m)
	})
	logger.Infof("starting server")
	server := &dns.Server{Addr: ":5353", Net: "udp", Handler: mux}
	server.ListenAndServe()
}

func main() {
	loggerCreate()
	serve()
}
